﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Maya : MonoBehaviour {

	public enum maya_state {
		IDLE,
		RUN,
		ATTACK,
		DEAD
	};

	private int hp;
	public int maxHp;
	public maya_state state = maya_state.IDLE;

	void Start () {
		hp = maxHp;
		this.GetComponent<NavMeshAgent> ().SetDestination(new Vector3(106f, 1.4f, 106f));
	}
	
	void Update () {
		if (hp <= 0)
			this.state = maya_state.DEAD;
		gameObject.GetComponent<Animator> ().SetInteger ("State", (int)state);
	}
}
