﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

	public GameObject player;

	void Start () {
		
	}
	
	void Update () {
		Vector3 playerPos = player.transform.position;
		playerPos.x -= 10f;
		playerPos.y += 10f;
		this.transform.position = playerPos;
	}
}
